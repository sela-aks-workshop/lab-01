# Kubernetes Workshop
Lab 01: Setting Up Your Workstation

---

## Prerequisites

 - Get the IP address of your workstation from the instructor
 - Workstation Credentials:
   - Username: Sela
   - Password: Sela1234!

## Instructions

 - Connect to the VM using SSH using the IP address provided by the instructor
```
ssh sela@<server-ip>
```

 - Install 'kubectl' using snap
```
sudo snap install kubectl --classic
```

 - Ensure kubectl is installed and check it version
```
kubectl version
```

 - Check the kubectl configuration
```
cat ~/.kube/config
```

 - Inspect the current context
```
kubectl config get-contexts
```

 - Ensure that you have access to your cluster (list nodes)
```
kubectl get nodes
```

 - Inspect node details
```
kubectl describe node <node-name>
```

 - Get all cluster resources
```
kubectl get all --all-namespaces
```